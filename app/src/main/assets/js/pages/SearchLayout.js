import React from "react";
import { Link } from "react-router";
import InfiniteScroll from 'react-infinite-scroll-component';

var $ = require('jquery');
var he = require('he');


export default class SearchLayout extends React.Component {

	constructor () {
	  super();
	  this.state = {
	    resDivs: [],
	    hasMore: true,
	    itemPerPage: 10,
	    page: 1,
	    totalRecords: ""
	  };
	  this.generateDivs = this.generateDivs.bind(this);
	}

	componentDidMount() {
		this.setState({resDivs: []});
        this.generateDivs();
    }

	componentDidUpdate(prevProps, prevState){
		if(this.props.params.query !== prevProps.params.query){
			this.setState({resDivs: []});
			this.generateDivs();
		}
	}

	generateDivs () {
      let moreResults = [];
      var skusRes = [];     
      let count = this.state.itemPerPage;
      var self = this;
      var no = this.state.itemPerPage * this.state.page;
      var searchTerm = this.props.params.query.replace("_", " ");
      searchTerm = searchTerm.trim();

      $.ajax({
		  url: 'https://www.officedepot.com/mobile/search.do?N=5&Ntt='+searchTerm+'&recordsPerPageNumber='+this.state.itemPerPage+'&No='+no,
		  dataType: 'JSON',
		  type: 'POST',
		  crossDomain: true,
		  success: function () { },
		  error: function () { console.log("error"); },
		  complete: function (data) {
		      if (data.readyState == '4' && data.status == '200') {
		          var results = JSON.parse(data.responseText);
				  skusRes = results.skus;
				  self.setState({totalRecords: results.totalRecords});
				  if(Math.floor(skusRes.totalRecords/self.state.itemPerPage) === self.state.page){
				  	self.setState({hasMore: false});
				  }
				  if(skusRes.length !== 0){
					  for (let i = 0; i < count; i++) {

							moreResults.push(
					          <li key={skusRes[i].sku} class="product">
					          	<div class="image_reviews">
					            	<img src={skusRes[i].productImagesMobile.defaultMediumImageURL} />
					            </div>
					            <div class="sku_details">
						            <div class="vpadding_bottom_half"><span class="sku_description black_text vpadding_bottom_half">{he.decode(skusRes[i].description)}</span></div>
						            <div class="review_sections vpadding_bottom_half">
							            <span>Rating: {skusRes[i].rating.toFixed()} </span>
							            <span class="numberRatings">Reviews: {skusRes[i].totalReviews}</span>
						            </div>					            
						            <div class="review_price">
						              <div class="sku_pricing_v2">
							            <p class="sale_price"><span class="bold">{skusRes[i].sellPrice}</span>
							            <span class="grey_price_text uom">/&nbsp;{skusRes[i].uom}</span></p>
						              </div>
						              <div class="sku_btn">					       	        
										<div>
											<button type="submit" class="button b1" title="Add to Cart">Add to Cart</button>
										</div>
									  </div>
						            </div>
					          	</div>
					          </li>
					        );

					  }
				  }else{
				  	moreResults.push(<h4>No results</h4>);
				  }
				  self.setState({resDivs: self.state.resDivs.concat(moreResults)});
		      }else{
		      	console.log("can't get search results");
		      }
		  }		  
	  });
	  self.setState({page: self.state.page + 1});
	
    }  

	render() {
		var query = this.props.params.query;
		var self = this;
		return (
			<div style={{marginTop:"35%"}}>
				<p style={{paddingLeft:"10px"}}>You searched for: {query.replace("_"," ")} <br/> Total Records: {this.state.totalRecords}</p>
				<ul style={{paddingLeft:"10px"}}>
				<InfiniteScroll
				  scrollThreshold={0.2}
		          next={this.generateDivs}
		          hasMore={this.state.hasMore}
		          loader={<h4 style={{textAlign:"center",paddingRight:"20px"}}><i class="fa fa-spinner fa-spin" style={{fontSize:"24px"}}></i></h4>}>
		          {this.state.resDivs}
		        </InfiniteScroll>
		        </ul>
			</div>
		);
	}
}