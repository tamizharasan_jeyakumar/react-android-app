import React from "react";
import { Link } from "react-router";

import BelowList from "../components/BelowList";
import Footer from "../components/Footer";
import List from "../components/List";

export default class Layout extends React.Component {	
	render() {
		return (
			<div>				
				<List />
				<BelowList />
				<Footer />
			</div>
		);
	}
}