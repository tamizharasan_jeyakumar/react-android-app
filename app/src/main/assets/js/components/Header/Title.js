import React from "react";

import Search from "./Search";

export default class Title extends React.Component {
	render() {
		var headerImg = "img/headder.png";		
		return (
		  <div style={{position:'fixed',
					top: '0px',
					left: '0px',
					width: '100%'}}>
			<img src={headerImg} style={{width: '100%'}} />
			<Search />
		  </div>
		);
	}
}