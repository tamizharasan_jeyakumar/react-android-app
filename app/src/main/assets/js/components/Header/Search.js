import React from "react";
import { Link } from "react-router";

import Autosuggest from 'react-autosuggest';

var $ = require('jquery');

var products = [];

const getSuggestions = value => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;

  return inputLength === 0 ? [] : products.filter(lang =>
    lang.resultText.toLowerCase().slice(0, inputLength) === inputValue
  );
};

const getSuggestionValue = suggestion => suggestion.resultText;

const renderSuggestion = suggestion => (
  <div style={{color:"white"}}>
    {suggestion.resultText}
  </div>
);

export default class Search extends React.Component {

	constructor() {
	  super();
	  this.state = {
	    value: '',
	    suggestions: []
	  };
	}

	onChange = (event, { newValue }) => {
	  this.setState({
	    value: newValue
	  });
	  	var self = this;

	    if(newValue.length > 1){
	      $.ajax({
	          url: 'https://www.officedepot.com/mobile/predictiveSearch.do?searchTerm=' + newValue,
	          dataType: 'JSON',
	          type: 'POST',
	          crossDomain: true,
	          success: function () { },
	          error: function () { console.log("error"); },
	          complete: function (data) {
	              if (data.readyState == '4' && data.status == '200') {
	                  console.log("Suggestions retrieved");
	                  var result = JSON.parse(data.responseText);
	                  products = result.predictiveSearch;
	              }else{
	                console.log("error");
	              }
	          }
	      });
	    }
	};

	onSuggestionsFetchRequested = ({ value }) => {
	  this.setState({
	    suggestions: getSuggestions(value)
	  });
	};

	onSuggestionsClearRequested = () => {
	  this.setState({
	    suggestions: []
	  });
	};

	render() {
		const { value, suggestions } = this.state;

        const inputProps = {
          placeholder: 'Search Product or Item Number',
          value,
          onChange: this.onChange
        };
		var type = "submit";
		var valueOfSearch = "Search";
		var divStyle = {
						backgroundColor:"#000",
						padding: "5px",
						width: '100%'
					   };
		var searchQuery = "search\/" + value.trim().replace(" ","_");

		return (
			<div>
				<form>
					<div style={divStyle}>
						<Autosuggest
					      suggestions={suggestions}
					      onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
					      onSuggestionsClearRequested={this.onSuggestionsClearRequested}
					      getSuggestionValue={getSuggestionValue}
					      renderSuggestion={renderSuggestion}
					      inputProps={inputProps}
					    />
					    <Link to={searchQuery}><input class="searchBtn" type={type} value={valueOfSearch}/></Link>						
					</div>					
				</form>
			</div>
		);
	}
}