import React from "react";

export default class BelowList extends React.Component {

    render() {
		var belowlistImg = "img/belowlist.png";
		return (
			<img src={belowlistImg} style={{width: '100%'}} />
		);
	}
}