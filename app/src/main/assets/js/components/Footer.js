import React from "react";

export default class Footer extends React.Component {
	
	render() {
		var footerImg = "img/footer.png";
		return (
			<img src={footerImg} style={{width: '100%'}} />
		);
	}
}