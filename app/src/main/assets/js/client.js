import React from "react";
import ReactDOM from "react-dom";
import Home from "./pages/Home";
import Layout from "./pages/Layout";
import SearchLayout from "./pages/SearchLayout";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

var $ = require('jquery');

$.ajax({
    url: 'https://www.officedepot.com/mobile/initApp.do?postalCode=',
    dataType: 'JSON',
    type: 'POST',
    crossDomain: true,
    success: function () { },
    error: function () { console.log("error"); },
    complete: function (data) {
        if (data.readyState == '4' && data.status == '200') {
            console.log("Session connected");
        }else{
        	console.log("can't connect to session");
        }
    }
});

const app = document.getElementById('app');

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={Home}></IndexRoute>
      <Route path="search(/:query)" name="search" component={SearchLayout}></Route>
    </Route>
  </Router>,
app);