package com.wipro.technovation.reactapp;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends Activity {

    private WebView wvReactView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        wvReactView = (WebView) findViewById(R.id.wvReactView);
        wvReactView.getSettings().setJavaScriptEnabled(true); //Telling the WebView to enable JavaScript execution
        wvReactView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        wvReactView.getSettings().setBuiltInZoomControls(false);
        wvReactView.getSettings().setSupportZoom(false);
        wvReactView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        wvReactView.setVerticalScrollBarEnabled(false);
        //wvReactView.loadUrl("https://www.officedepot.com");
        wvReactView.loadUrl("file:///android_asset/index.html");

        // setting the webView client.This clint helps to handle navigation inside the WebView
        WebViewClientManager webViewClient = new WebViewClientManager(MainActivity.this);
        wvReactView.setWebViewClient(webViewClient);
        wvReactView.setWebChromeClient(new MyWebViewClient());

    }

    @Override
    public void onBackPressed() {
        if (wvReactView.isFocused() && wvReactView.canGoBack()) {
            wvReactView.goBack();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    private class MyWebViewClient extends WebChromeClient {
        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            Log.d("React", "Console Message: " + consoleMessage.message() + " -- From line "
                    + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
            return super.onConsoleMessage(consoleMessage);
        }

    }
}
